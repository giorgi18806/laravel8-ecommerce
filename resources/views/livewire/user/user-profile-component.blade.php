<div>
    <div class="container" style="padding: 30px 0">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ __('profile.profile') }}
                </div>
                <div class="panel-body">
                    <div class="col-md-4">
                        @if($user->profile->image)
                            <img src="{{ asset('assets/images/profile') }}/{{ $user->profile->image }}" alt="user" width="100%">
                        @else
                            <img src="{{ asset('assets/images/profile/default.png') }}" alt="default" width="100%">
                        @endif
                    </div>
                    <div class="col-md-8">
                        <p><b>{{ __('profile.name') }}: </b>{{ $user->name }}</p>
                        <p><b>{{ __('profile.email') }}: </b>{{ $user->email }}</p>
                        <p><b>{{ __('profile.phone') }}: </b>{{ $user->profile->mobile }}</p>
                        <hr>
                        <p><b>{{ __('profile.address') }}: </b>{{ $user->profile->line1 }}</p>
                        <p><b>{{ __('profile.address') }}: </b>{{ $user->profile->line2 }}</p>
                        <p><b>{{ __('profile.city') }}: </b>{{ $user->profile->city }}</p>
                        <p><b>{{ __('profile.province') }}: </b>{{ $user->profile->province }}</p>
                        <p><b>{{ __('profile.country') }}: </b>{{ $user->profile->country }}</p>
                        <p><b>{{ __('profile.zipcode') }}: </b>{{ $user->profile->zipcode }}</p>
                        <a href="{{ route('user.profile.edit') }}" class="btn btn-info pull-right">{{ __('profile.update-profile') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
