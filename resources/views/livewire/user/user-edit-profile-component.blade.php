<div>
    <div class="container" style="padding: 30px 0">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ __('profile.update-profile') }}
                </div>
                <div class="panel-body">
                    @if(Session::has('message'))
                        <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                    @endif
                    <form wire:submit.prevent="updateProfile">
                        <div class="col-md-4">
                            @if($new_image)
                                <img src="{{ $new_image->temporaryUrl() }}" alt="user" width="100%">
                            @elseif($image)
                                <img src="{{ asset('assets/images/profile') }}/{{ $image }}" alt="user" width="100%">
                            @else
                                <img src="{{ asset('assets/images/profile/default.png') }}" alt="default" width="100%">
                            @endif
                            <input type="file" class="form-control" wire:model="new_image">
                        </div>
                        <div class="col-md-8">
                            <p><b>{{ __('profile.name') }}: </b><input type="text" class="form-control" wire:model="name"></p>
                            <p><b>{{ __('profile.email') }}: </b>{{ $email }}</p>
                            <p><b>{{ __('profile.phone') }}: </b><input type="text" class="form-control" wire:model="mobile"></p>
                            <hr>
                            <p><b>{{ __('profile.address') }}: </b><input type="text" class="form-control" wire:model="line1"></p>
                            <p><b>{{ __('profile.address') }}: </b><input type="text" class="form-control" wire:model="line2"></p>
                            <p><b>{{ __('profile.city') }}: </b><input type="text" class="form-control" wire:model="city"></p>
                            <p><b>{{ __('profile.province') }}: </b><input type="text" class="form-control" wire:model="province"></p>
                            <p><b>{{ __('profile.country') }}: </b><input type="text" class="form-control" wire:model="country"></p>
                            <p><b>{{ __('profile.zipcode') }}: </b><input type="text" class="form-control" wire:model="zipcode"></p>
                            <button type="submit" class="btn btn-info pull-right">{{ __('profile.update') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
