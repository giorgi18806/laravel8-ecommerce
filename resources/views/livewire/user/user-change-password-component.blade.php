<div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ __('change-password.change-password') }}
                    </div>
                    <div class="panel-body">
                        @if(Session::has('password_success'))
                            <div class="alert alert-success" role="alert">{{ Session::get('password_success') }}</div>
                        @endif
                        @if(Session::has('password_error'))
                            <div class="alert alert-danger" role="alert">{{ Session::get('password_error') }}</div>
                        @endif
                        <form class="form-horizontal" wire:submit.prevent="changePassword">
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('change-password.current-password') }}</label>
                                <div class="col-md-4">
                                    <input type="password" class="form-control input-md" name="current_password" placeholder="Current Password" wire:model="current_password">
                                    @error('current_password') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('change-password.new-password') }}</label>
                                <div class="col-md-4">
                                    <input type="password" class="form-control input-md" name="password" placeholder="New Password" wire:model="password">
                                    @error('password') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('change-password.confirm-password') }}</label>
                                <div class="col-md-4">
                                    <input type="password" class="form-control input-md" name="password_confirmation" placeholder="Confirm Password" wire:model="password_confirmation">
                                    @error('password_confirmation') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">{{ __('change-password.submit') }}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
