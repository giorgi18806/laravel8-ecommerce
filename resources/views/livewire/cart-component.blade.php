<main id="main" class="main-site">

    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="/" class="link">{{ __('cart.home') }}</a></li>
                <li class="item-link"><span>{{ __('cart.cart') }}</span></li>
            </ul>
        </div>
        <div class=" main-content-area">
            @if(Cart::instance('cart')->count() > 0)
                <div class="wrap-iten-in-cart">
                    @if(Session::has('success_message'))
                        <div class="alert alert-success">
                            <span>{{ Session::get('success_message') }}</span>
                        </div>
                    @endif
                    <h3 class="box-title">{{ __('cart.product-name') }}</h3>
                    <ul class="products-cart">
    {{--                    @dd(Cart::instance('cart')->content())--}}
                        @forelse(Cart::instance('cart')->content() as $item)
                            <li class="pr-cart-item">
                                <div class="product-image">
                                    <figure><img src="{{ asset('assets/images/products') }}/{{ $item->model->image }}" alt="{{ $item->model->name }}"></figure>
                                </div>
                                <div class="product-name">
                                    <a class="link-to-product" href="{{ route('product-details', $item->model->slug) }}">{{ $item->model->name }}</a>
                                </div>
                                з@foreach($item->options as $key => $value)
                                    <div style="vertical-align: middle; width: 180px">
                                        <p><b>{{ $key }}: {{ $value }}</b></p>
                                    </div>
                                @endforeach
                                <div class="price-field produtc-price"><p class="price">${{ $item->model->regular_price }}</p></div>
                                <div class="quantity">
                                    <div class="quantity-input">
                                        <input type="text" name="product-quantity" value="{{ $item->qty }}" data-max="120" pattern="[0-9]*" >
                                        <a class="btn btn-increase" href="#" wire:click.prevent="increaseQuantity('{{ $item->rowId }}')"></a>
                                        <a class="btn btn-reduce" href="#" wire:click.prevent="decreaseQuantity('{{ $item->rowId }}')"></a>
                                    </div>
                                    <p class="text-center"><a href="#" wire:click.prevent="switchToSaveForLater('{{ $item->rowId }}')">{{ __('cart.save-for-later') }}</a></p>
                                </div>
                                <div class="price-field sub-total"><p class="price">${{ $item->subtotal }}</p></div>
                                <div class="delete">
                                    <a href="#" class="btn btn-delete" title="" wire:click.prevent="destroy('{{ $item->rowId }}')">
                                        <span>{{ __('product-delete') }}</span>
                                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </li>
                        @empty
                            <p>{{ __('cart.no-items') }}</p>
                        @endforelse
                    </ul>
                </div>

                <div class="summary">
                <div class="order-summary">
                    <h4 class="title-box">{{ __('cart.summary') }}</h4>
                    <p class="summary-info"><span class="title">{{ __('cart.subtotal') }}</span><b class="index">${{Cart::instance('cart')->subtotal()}}</b></p>
                    @if(Session::has('coupon'))
                        <p class="summary-info"><span class="title">{{ __('cart.discount') }} ({{ Session::get('coupon')['code'] }})<a href="#" wire:click.prevent="removeCoupon"><i class="fa fa-times text-danger"></i></a></span><b class="index"> -${{ number_format($discount, 2) }}</b></p>
                        <p class="summary-info"><span class="title">{{ __('cart.subtotal-discount') }}</span><b class="index">${{ number_format($subtotalAfterDiscount, 2) }}</b></p>
                        <p class="summary-info"><span class="title">{{ __('cart.tax') }} ({{ config('cart.tax') }}%)</span><b class="index">${{ number_format($taxAfterDiscount, 2) }}</b></p>
                        <p class="summary-info total-info "><span class="title">Total</span><b class="index">${{ number_format($totalAfterDiscount, 2) }}</b></p>
                    @else
                        <p class="summary-info"><span class="title">{{ __('cart.tax') }}</span><b class="index">${{Cart::instance('cart')->tax()}}</b></p>
                        <p class="summary-info"><span class="title">{{ __('cart.shipping') }}</span><b class="index">{{ __('cart.free-shipping') }}</b></p>
                        <p class="summary-info total-info "><span class="title">{{ __('cart.total') }}</span><b class="index">${{Cart::instance('cart')->total()}}</b></p>
                    @endif
                </div>
                    <div class="checkout-info">
                        @if(!Session::has('coupon'))
                            <label class="checkbox-field">
                                <input class="frm-input " name="have-code" id="have-code" value="1" type="checkbox" wire:model="haveCouponCode"><span>{{ __('cart.have-coupon') }}</span>
                            </label>
                            @if($haveCouponCode == 1)
                                <div class="summary-item">
                                    <form wire:submit.prevent="applyCouponCode">
                                        <h4 class="title-box">{{ __('cart.coupon-code') }}</h4>
                                        @if(Session::has('coupon_message'))
                                            <div class="alert alert-danger" role="danger">{{ Session::get('coupon_message') }}</div>
                                        @endif
                                        <p class="row-in-form">
                                            <label for="coupon-code">{{ __('cart.enter-code') }}:</label>
                                            <input type="text" name="coupon-code" wire:model="couponCode">
                                        </p>
                                        <button type="submit" class="btn btn-small">{{ __('cart.apply') }}</button>
                                    </form>
                                </div>
                            @endif
                        @endif
                        <a class="btn btn-checkout" href="#" wire:click.prevent="checkout">{{ __('cart.checkout') }}</a>
                        <a class="link-to-shop" href="{{ route('shop') }}">{{ __('cart.continue-shopping') }}<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    </div>
                <div class="update-clear">
                    <a class="btn btn-clear" href="#" wire:click.prevent="destroyAll()">{{ __('cart.clear-cart') }}</a>
                    <a class="btn btn-update" href="#">{{ __('cart.update-cart') }}</a>
                </div>
            </div>
            @else
                <div class="text-center" style="padding: 30px 0">
                    <h1>{{ __('cart.empty-cart') }}</h1>
                    <p>{{ __('cart.add-items-cart') }}</p>
                    <a href="/shop" class="btn" style="color: #ffffff;background-color: #ff2832;">{{ __('cart.shop-now') }}</a>
                </div>
            @endif
            <div class="wrap-iten-in-cart">
                <h3 class="title-box" style="border-bottom: 1px solid; padding-bottom: 15px;">{{ Cart::instance('saveForLater')->count() }} {{ __('cart.saved-for-later') }}</h3>
                @if(Session::has('s_success_message'))
                    <div class="alert alert-success">
                        <span>{{ Session::get('s_success_message') }}</span>
                    </div>
                @endif
                <h3 class="box-title">{{ __('cart.product-name') }}</h3>
                <ul class="products-cart">
                    {{--                    @dd(Cart::instance('cart')->content())--}}
                    @forelse(Cart::instance('saveForLater')->content() as $item)
                        <li class="pr-cart-item">
                            <div class="product-image">
                                <figure><img src="{{ asset('assets/images/products') }}/{{ $item->model->image }}" alt="{{ $item->model->name }}"></figure>
                            </div>
                            <div class="product-name">
                                <a class="link-to-product" href="{{ route('product-details', $item->model->slug) }}">{{ $item->model->name }}</a>
                            </div>
                            <div class="price-field produtc-price"><p class="price">${{ $item->model->regular_price }}</p></div>
                            <div class="quantity">
                                <p class="text-center"><a href="#" wire:click.prevent="moveToCart('{{ $item->rowId }}')">{{ __('cart.move-to-cart') }}</a></p>
                            </div>
                            <div class="delete">
                                <a href="#" class="btn btn-delete" title="" wire:click.prevent="deleteFromSaveForLater('{{ $item->rowId }}')">
                                    <span>{{ __('cart.delete-saved') }}</span>
                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                </a>
                            </div>
                        </li>
                    @empty
                        <p>{{ __('cart.no-saved') }}</p>
                    @endforelse
                </ul>
            </div>

            <div class="wrap-show-advance-info-box style-1 box-in-site">
                <h3 class="title-box">{{ __('cart.most-viewed-products') }}</h3>
                <div class="wrap-products">
                    <div class="products slide-carousel owl-carousel style-nav-1 equal-container" data-items="5" data-loop="false" data-nav="true" data-dots="false" data-responsive='{"0":{"items":"1"},"480":{"items":"2"},"768":{"items":"3"},"992":{"items":"3"},"1200":{"items":"5"}}' >

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_04.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item new-label">{{ __('cart.new') }}</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">{{ __('cart.quick-view') }}</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_17.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item sale-label">{{ __('cart.sale') }}</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">{{ __('cart.quick-view') }}</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><ins><p class="product-price">$168.00</p></ins> <del><p class="product-price">$250.00</p></del></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_15.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item new-label">{{ __('cart.new') }}</span>
                                    <span class="flash-item sale-label">{{ __('cart.sale') }}</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">{{ __('cart.quick-view') }}</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><ins><p class="product-price">$168.00</p></ins> <del><p class="product-price">$250.00</p></del></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_01.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item bestseller-label">{{ __('cart.bestseller') }}</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">{{ __('cart.quick-view') }}</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_21.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">{{ __('cart.quick-view') }}</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_03.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item sale-label">{{ __('cart.sale') }}</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">{{ __('cart.quick-view') }}</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><ins><p class="product-price">$168.00</p></ins> <del><p class="product-price">$250.00</p></del></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_04.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item new-label">{{ __('cart.new') }}</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">{{ __('cart.quick-view') }}</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_05.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item bestseller-label">{{ __('cart.bestseller') }}</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">{{ __('cart.quick-view') }}</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>
                    </div>
                </div><!--End wrap-products-->
            </div>

        </div><!--end main content area-->
    </div><!--end container-->

</main>
