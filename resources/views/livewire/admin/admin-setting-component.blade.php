<div>
    <div class="container" style="padding: 30px 0">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ __('settings.settings') }}
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <form class="form-horizontal" wire:submit.prevent="saveSettings">
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.email') }}</label>
                                <div class="col-md-4">
                                    <input type="email" placeholder="{{ __('settings.email') }}" class="form-control input-md" wire:model="email">
                                    @error('email') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.phone') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('settings.phone') }}" class="form-control input-md" wire:model="phone">
                                    @error('phone') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.phone2') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('settings.phone2') }}" class="form-control input-md" wire:model="phone2">
                                    @error('phone2') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.address') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('settings.address') }}" class="form-control input-md" wire:model="address">
                                    @error('address') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.map') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('settings.map') }}" class="form-control input-md" wire:model="map">
                                    @error('map') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.twitter') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('settings.twitter') }}" class="form-control input-md" wire:model="twitter">
                                    @error('twitter') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.facebook') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('settings.facebook') }}" class="form-control input-md" wire:model="facebook">
                                    @error('facebook') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.pinterest') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('settings.pinterest') }}" class="form-control input-md" wire:model="pinterest">
                                    @error('pinterest') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.instagram') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('settings.instagram') }}" class="form-control input-md" wire:model="instagram">
                                    @error('instagram') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('settings.youtube') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('settings.youtube') }}" class="form-control input-md" wire:model="youtube">
                                    @error('youtube') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">{{ __('settings.submit') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
