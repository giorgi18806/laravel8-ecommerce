<div>
    <style>
        nav svg {
            height: 20px;
        }
        nav .hidden {
            display: block !important;
        }
        .sclist {
            list-style: none;
        }
        .sclist li {
            line-height: 33px;
            border-bottom: 1px solid #ccc;
        }
        .slink {
            font-size: 16px;
            margin-left: 3px;
        }
    </style>
    <div class="container" style="padding: 30px 0;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                {{ __('admin-attributes.all-attributes') }}
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.attribute.add') }}" class="btn btn-success pull-right">{{ __('admin-attributes.add-new') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>{{ __('admin-attributes.id') }}</th>
                                <th>{{ __('admin-attributes.name') }}</th>
                                <th>{{ __('admin-attributes.created_at') }}</th>
                                <th>{{ __('admin-attributes.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product_attributes as $attribute)
                                <tr>
                                    <td>{{ $attribute->id }}</td>
                                    <td>{{ $attribute->name }}</td>
                                    <td>{{ $attribute->created_at }}</td>
                                    <td>
                                        <a href="{{ route('admin.attribute.edit', $attribute->id) }}"><i class="fa fa-edit fa-2x"></i></a>
                                        <a href="#" onclick="confirm({{ __('admin-attributes.delete-attribute') }}) || event.stopImmediatePropagation()" wire:click.prevent="deleteAttribute({{ $attribute->id }})" style="margin-left: 10px"><i class="fa fa-times fa-2x text-danger"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $product_attributes->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
