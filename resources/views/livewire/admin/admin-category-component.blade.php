<div>
    <style>
        nav svg {
            height: 20px;
        }
        nav .hidden {
            display: block !important;
        }
        .sclist {
            list-style: none;
        }
        .sclist li {
            line-height: 33px;
            border-bottom: 1px solid #ccc;
        }
        .slink {
            font-size: 16px;
            margin-left: 3px;
        }
    </style>
    <div class="container" style="padding: 30px 0;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                {{ __('admin-categories.all-categories') }}
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.category.add') }}" class="btn btn-success pull-right">{{ __('admin-categories.add-new') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>{{ __('admin-categories.id') }}</th>
                                <th>{{ __('admin-categories.name') }}</th>
                                <th>{{ __('admin-categories.slug') }}</th>
                                <th>{{ __('admin-categories.sub_category') }}</th>
                                <th>{{ __('admin-categories.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->id }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->slug }}</td>
                                        <td>
                                            <ul class="sclist">
                                                @foreach($category->subCategories as $sub_category)
                                                    <li><i class="fa fa-caret-right"></i>{{ $sub_category->name }}
                                                        <a href="{{ route('admin.category.edit', ['category_slug' => $category->slug, 'sub_category_slug' => $sub_category->slug]) }}" class="slink">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <a href="#" onclick="confirm({{ __('admin-categories.delete-sub_category') }}) || event.stopImmediatePropagation()" wire:click.prevent="deleteSubcategory({{ $sub_category->id }})" class="slink"><i class="fa fa-times text-danger"></i></a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.category.edit', $category->slug) }}"><i class="fa fa-edit fa-2x"></i></a>
                                            <a href="#" onclick="confirm({{ __('admin-categories.delete-category') }}) || event.stopImmediatePropagation()" wire:click.prevent="deleteCategory({{ $category->id }})" style="margin-left: 10px"><i class="fa fa-times fa-2x text-danger"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
