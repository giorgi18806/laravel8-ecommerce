<div>
    <div class="container" style="padding: 30px 0;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                {{ __('product.admin.add-new-product') }}
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.products') }}" class="btn btn-success pull-right">{{ __('product.admin.all-products') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <form class="form-horizontal" enctype="multipart/form-data" wire:submit.prevent="storeProduct">
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.name') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('product.name') }}" class="form-control input-md" wire:model="name" wire:keyup="generateSlug">
                                    @error('name') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.slug') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('product.slug') }}" class="form-control input-md" wire:model="slug">
                                    @error('slug') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.short-description') }}</label>
                                <div class="col-md-4" wire:ignore>
                                    <textarea placeholder="{{ __('product.short-description') }}" id="short_description" class="form-control" wire:model="short_description"></textarea>
                                    @error('short_description') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.description') }}</label>
                                <div class="col-md-4" wire:ignore>
                                    <textarea placeholder="{{ __('product.description') }}" id="description" class="form-control" wire:model="description"></textarea>
                                    @error('description') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.regular-price') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('product.regular-price') }}" class="form-control input-md" wire:model="regular_price">
                                    @error('regular_price') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.sale-price') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('product.sale-price') }}" class="form-control input-md" wire:model="sale_price">
                                    @error('sale_pirce') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.sku') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('product.sku') }}" class="form-control input-md" wire:model="SKU">
                                    @error('SKU') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.stock') }}</label>
                                <div class="col-md-4">
                                    <select class="form-control" wire:model="stock_status">
                                        <option value="instock">{{ __('product.in-stock') }}</option>
                                        <option value="outofstock">{{ __('product.out-of-stock') }}</option>
                                    </select>
                                    @error('stock_status') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.featured') }}</label>
                                <div class="col-md-4">
                                    <select class="form-control" wire:model="featured">
                                        <option value="0">{{ __('product.no') }}</option>
                                        <option value="1">{{ __('product.yes') }}</option>
                                    </select>
                                    @error('featured') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.quantity') }}</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="{{ __('product.quantity') }}" class="form-control input-md" wire:model="quantity">
                                    @error('quantity') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.product-image') }}</label>
                                <div class="col-md-4">
                                    <input type="file" class="input-file" wire:model="image">
                                    @if($image)
                                        <img src="{{ $image->temporaryUrl() }}" width="120">
                                    @endif
                                    @error('image') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.product-gallery') }}</label>
                                <div class="col-md-4">
                                    <input type="file" class="input-file" wire:model="images" multiple>
                                    @if($images)
                                        @foreach($images as $image)
                                            <img src="{{ $image->temporaryUrl() }}" width="120">
                                        @endforeach
                                    @endif
                                    @error('images') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.category') }}</label>
                                <div class="col-md-4">
                                    <select class="form-control" wire:model="category_id" wire:change="changeSubCategory">
                                        <option value="">{{ __('product.select-category') }}</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('category_id') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.sub_category') }}</label>
                                <div class="col-md-4">
                                    <select class="form-control" wire:model="sub_category_id">
                                        <option value="">{{ __('product.select-sub_category') }}</option>
                                        @foreach($sub_categories as $sub_category)
                                            <option value="{{ $sub_category->id }}">{{ $sub_category->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('sub_category_id') <p class="text-danger">{{ $message }}</p> @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{ __('product.product-attributes') }}</label>
                                <div class="col-md-3">
                                    <select class="form-control" wire:model="attr">
                                        <option value="">{{ __('product.select-product-attributes') }}</option>
                                        @foreach($product_attributes as $product_attribute)
                                            <option value="{{ $product_attribute->id }}">{{ $product_attribute->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-info pull-right" wire:click.prevent="add">{{ __('product.admin.add') }}</button>
                                </div>
                            </div>
                            @foreach($inputs as $key => $value)
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{ $product_attributes->where('id', $attribute_arr[$key])->first()->name }}</label>
                                    <div class="col-md-3">
                                        <input type="text" placeholder="{{ $product_attributes->where('id', $attribute_arr[$key])->first()->name }}" class="form-control input-md" wire:model="attribute_values.{{ $value }}">
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-danger pull-right" wire:click.prevent="remove({{ $key }})">{{ __('product.admin.remove') }}</button>
                                    </div>
                                </div>
                            @endforeach

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">{{ __('product.submit') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function () {
            tinymce.init({
                selector: '#short_description',
                setup: function (editor) {
                    editor.on('Change', function (e) {
                        tinyMCE.triggerSave();
                        var sd_data = $('#short_description').val();
                        @this.set('short_description', sd_data);
                    })
                }
            });

            tinymce.init({
                selector: '#description',
                setup: function (editor) {
                    editor.on('Change', function (e) {
                        tinyMCE.triggerSave();
                        var d_data = $('#description').val();
                    @this.set('description', d_data);
                    })
                }
            });
        });
    </script>
@endpush
