<div class="content">
    <style>
        .content {
            padding-top: 40px;
            padding-bottom: 40px;
        }
        .icon-stat {
            display: block;
            overflow: hidden;
            position: relative;
            padding: 15px;
            margin-bottom: 1em;
            background-color: #fff;
            border-radius: 4px;
            border: 1px solid #ddd;
        }
        .icon-stat-label {
            display: block;
            color: #999;
            font-size: 13px;
        }
        .icon-stat-value {
            display: block;
            font-size: 28px;
            font-weight: 600;
        }
        .icon-stat-visual {
            position: relative;
            top: 22px;
            display: inline-block;
            width: 32px;
            height: 32px;
            border-radius: 4px;
            text-align: center;
            font-size: 16px;
            line-height: 30px;
        }
        .bg-primary {
            color: #fff;
            background: #d74b4b;
        }
        .bg-secondary {
            color: #fff;
            background: #6685a4;
        }

        .icon-stat-footer {
            padding: 10px 0 0;
            margin-top: 10px;
            color: #aaa;
            font-size: 12px;
            border-top: 1px solid #eee;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="icon-stat">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <span class="icon-stat-label">{{ __('dashboard.admin.total-revenue') }}</span>
                            <span class="icon-stat-value">₴{{ $totalRevenue }}</span>
                        </div>
                        <div class="col-xs-4 text-center">
                            <i class="fa fa-dollar icon-stat-visual bg-primary"></i>
                        </div>
                    </div>
                    <div class="icon-stat-footer">
                        <i class="fa fa-clock-o"></i> {{ __('dashboard.admin.updated-now') }}
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="icon-stat">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <span class="icon-stat-label">{{ __('dashboard.admin.total-sales') }}</span>
                            <span class="icon-stat-value">{{ $totalSales }}</span>
                        </div>
                        <div class="col-xs-4 text-center">
                            <i class="fa fa-gift icon-stat-visual bg-secondary"></i>
                        </div>
                    </div>
                    <div class="icon-stat-footer">
                        <i class="fa fa-clock-o"></i> {{ __('dashboard.admin.updated-now') }}
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="icon-stat">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <span class="icon-stat-label">{{ __('dashboard.admin.today-revenue') }}</span>
                            <span class="icon-stat-value">₴{{ $todayRevenue }}</span>
                        </div>
                        <div class="col-xs-4 text-center">
                            <i class="fa fa-dollar icon-stat-visual bg-primary"></i>
                        </div>
                    </div>
                    <div class="icon-stat-footer">
                        <i class="fa fa-clock-o"></i> {{ __('dashboard.admin.updated-now') }}
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="icon-stat">
                    <div class="row">
                        <div class="col-xs-8 text-left">
                            <span class="icon-stat-label">{{ __('dashboard.admin.today-sales') }}</span>
                            <span class="icon-stat-value">{{ $todaySales }}</span>
                        </div>
                        <div class="col-xs-4 text-center">
                            <i class="fa fa-gift icon-stat-visual bg-secondary"></i>
                        </div>
                    </div>
                    <div class="icon-stat-footer">
                        <i class="fa fa-clock-o"></i> {{ __('dashboard.admin.updated-now') }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ __('dashboard.admin.latest-order') }}
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>{{ __('dashboard.admin.order_Id') }}</th>
                                <th>{{ __('dashboard.admin.subtotal') }}</th>
                                <th>{{ __('dashboard.admin.discount') }}</th>
                                <th>{{ __('dashboard.admin.tax') }}</th>
                                <th>{{ __('dashboard.admin.total') }}</th>
                                <th>{{ __('dashboard.admin.first-name') }}</th>
                                <th>{{ __('dashboard.admin.last-name') }}</th>
                                <th>{{ __('dashboard.admin.mobile') }}</th>
                                <th>{{ __('dashboard.admin.email') }}</th>
                                <th>{{ __('dashboard.admin.zipcode') }}</th>
                                <th>{{ __('dashboard.admin.status') }}</th>
                                <th>{{ __('dashboard.admin.order-date') }}</th>
                                <th class="text-center">{{ __('dashboard.admin.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>₴{{ $order->subtotal }}</td>
                                    <td>₴{{ $order->discount }}</td>
                                    <td>₴{{ $order->tax }}</td>
                                    <td>₴{{ $order->total }}</td>
                                    <td>{{ $order->first_name }}</td>
                                    <td>{{ $order->last_name }}</td>
                                    <td>{{ $order->mobile }}</td>
                                    <td>{{ $order->email }}</td>
                                    <td>{{ $order->zip_code }}</td>
                                    <td>{{ $order->status }}</td>
                                    <td>{{ $order->created_at }}</td>
                                    <td><a href="{{ route('admin.order.details', ['order_id' => $order->id]) }}" class="btn btn-info btn-sm">{{ __('dashboard.admin.details') }}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
