<div>
   <div class="container" style="padding: 30px 0;">
       <div class="row">
           <div class="col-md-12">
               <div class="panel panel-default">
                   <div class="panel-heading">
                       <div class="row">
                           <div class="col-md-6">
                               {{ __('order-details.ordered_items.order-details') }}
                           </div>
                           <div class="col-md-6">
                               <a href="{{ route('admin.orders') }}" class="btn btn-success pull-right">{{ __('order-details.ordered_items.all-orders') }}</a>
                           </div>
                       </div>
                   </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>{{ __('order-details.ordered_items.order-id') }}</th>
                                <td>{{ $order->id }}</td>
                                <th>{{ __('order-details.ordered_items.order-date') }}</th>
                                <td>{{ $order->created_at }}</td>
                                <th>{{ __('order-details.ordered_items.order-status') }}</th>
                                <td><span class="{{ ($order->status == 'delivered') ? 'bg-success' : 'bg-danger'}}">{{ $order->status }}</span></td>
                                @if($order->status == 'delivered')
                                    <th>{{ __('order-details.ordered_items.delivered-date') }}</th>
                                    <td>{{ $order->delivered_date }}</td>
                                @elseif($order->status == 'canceled')
                                    <th>{{ __('order-details.ordered_items.canceled-date') }}</th>
                                    <td>{{ $order->canceled_date }}</td>
                                @endif
                            </tr>
                        </table>
                    </div>
               </div>
           </div>
       </div>
       <div class="row">
           <div class="col-md-12">
               <div class="panel panel-default">
                   <div class="panel-heading">
                       <div class="row">
                           <div class="col-md-6">
                               {{ __('order-details.ordered_items.ordered-items') }}
                           </div>
                           <div class="col-md-6">

                           </div>
                       </div>
                   </div>
                   <div class="panel-body">
                       <div class="wrap-iten-in-cart">
                           <h3 class="box-title">{{ __('cart.product-name') }}</h3>
                           <ul class="products-cart">
                               @foreach($order->orderItems as $item)
                                   <li class="pr-cart-item">
                                       <div class="product-image">
                                           <figure><img src="{{ asset('assets/images/products') }}/{{ $item->product->image }}" alt="{{ $item->product->name }}"></figure>
                                       </div>
                                       <div class="product-name">
                                           <a class="link-to-product" href="{{ route('product-details', $item->product->slug) }}">{{ $item->product->name }}</a>
                                       </div>
                                       @if($item->options)
                                           <div class="product-name">
                                               @foreach(unserialize($item->options) as $key => $value)
                                                   <p><b>{{ $key }}: {{ $value }}</b></p>
                                               @endforeach
                                           </div>
                                       @endif
                                       <div class="price-field produtc-price"><p class="price">${{ $item->price }}</p></div>
                                       <div class="quantity">
                                           <h5>{{ $item->quantity }}</h5>
                                       </div>
                                       <div class="price-field sub-total"><p class="price">${{ $item->price * $item->quantity }}</p></div>
                                   </li>
                               @endforeach
                           </ul>
                       </div>
                       <div class="summary">
                           <div class="order-summary">
                               <h4 class="title-box">{{ __('order-details.ordered_items.order-summary') }}</h4>
                               <p class="summary-info"><span class="title">{{ __('order-details.ordered_items.order-subtotal') }}</span><b class="index">{{ $order->subtotal }}</b></p>
                               <p class="summary-info"><span class="title">{{ __('order-details.ordered_items.order-tax') }}</span><b class="index">{{ $order->tax }}</b></p>
                               <p class="summary-info"><span class="title">{{ __('order-details.ordered_items.order-shipping') }}</span><b class="index">Free Shipping</b></p>
                               <p class="summary-info"><span class="title">{{ __('order-details.ordered_items.order-total') }}</span><b class="index">{{ $order->total }}</b></p>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>

       <div class="row">
           <div class="col-md-12">
               <div class="panel panel-default">
                   <div class="panel-heading">
                       {{ __('order-details.billing_details.billing-details') }}
                   </div>
                   <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>{{ __('order-details.billing_details.first-name') }}</th>
                                <td>{{ $order->first_name }}</td>
                                <th>{{ __('order-details.billing_details.last-name') }}</th>
                                <td>{{ $order->last_name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order-details.billing_details.phone') }}</th>
                                <td>{{ $order->mobile }}</td>
                                <th>{{ __('order-details.billing_details.email') }}</th>
                                <td>{{ $order->email }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order-details.billing_details.line1') }}</th>
                                <td>{{ $order->line1 }}</td>
                                <th>{{ __('order-details.billing_details.line2') }}</th>
                                <td>{{ $order->line2 }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order-details.billing_details.city') }}</th>
                                <td>{{ $order->city }}</td>
                                <th>{{ __('order-details.billing_details.province') }}</th>
                                <td>{{ $order->province }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order-details.billing_details.country') }}</th>
                                <td>{{ $order->country }}</td>
                                <th>{{ __('order-details.billing_details.zipcode') }}</th>
                                <td>{{ $order->zip_code }}</td>
                            </tr>
                        </table>
                   </div>
               </div>
           </div>
       </div>

       @if($order->is_shipping_different)
           <div class="row">
               <div class="col-md-12">
                   <div class="panel panel-default">
                       <div class="panel-heading">
                           {{ __('order-details.shipping_details.shipping-details') }}
                       </div>
                       <div class="panel-body">
                           <table class="table">
                               <tr>
                                   <th>{{ __('order-details.billing_details.first-name') }}</th>
                                   <td>{{ $order->shipping->first_name }}</td>
                                   <th>{{ __('order-details.billing_details.last-name') }}</th>
                                   <td>{{ $order->shipping->last_name }}</td>
                               </tr>
                               <tr>
                                   <th>{{ __('order-details.billing_details.phone') }}</th>
                                   <td>{{ $order->shipping->mobile }}</td>
                                   <th>{{ __('order-details.billing_details.email') }}</th>
                                   <td>{{ $order->shipping->email }}</td>
                               </tr>
                               <tr>
                                   <th>{{ __('order-details.billing_details.line1') }}</th>
                                   <td>{{ $order->shipping->line1 }}</td>
                                   <th>{{ __('order-details.billing_details.line2') }}</th>
                                   <td>{{ $order->shipping->line2 }}</td>
                               </tr>
                               <tr>
                                   <th>{{ __('order-details.billing_details.city') }}</th>
                                   <td>{{ $order->shipping->city }}</td>
                                   <th>{{ __('order-details.billing_details.province') }}</th>
                                   <td>{{ $order->shipping->province }}</td>
                               </tr>
                               <tr>
                                   <th>{{ __('order-details.billing_details.country') }}</th>
                                   <td>{{ $order->shipping->country }}</td>
                                   <th>{{ __('order-details.billing_details.zipcode') }}</th>
                                   <td>{{ $order->shipping->zip_code }}</td>
                               </tr>
                           </table>
                       </div>
                   </div>
               </div>
           </div>
       @endif

       <div class="row">
           <div class="col-md-12">
               <div class="panel panel-default">
                   <div class="panel-heading">
                        {{ __('order-details.transaction.transaction') }}
                   </div>
                   <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>Transation Mode</th>
                                <td>{{ $order->transaction->mode }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{ $order->transaction->status }}</td>
                            </tr>
                            <tr>
                                <th>Transation Date</th>
                                <td>{{ $order->transaction->created_at }}</td>
                            </tr>
                        </table>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
