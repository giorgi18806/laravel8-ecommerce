<main id="main" class="main-site left-sidebar">

    <style>
        #slider .noUi-connect {
            background: #c0392b;
        }

        .product-wish {
            position: absolute;
            top: 10%;
            left: 0;
            z-index: 99;
            right: 30px;
            text-align: right;
            padding-top: 0;
        }
        .product-wish .fa {
            color: #cbcbcb;
            font-size: 32px;
        }
        .product-wish .fa:hover {
            color: #ff7007;
        }
        .fill-heart {
            color: #ff7007 !important;
        }
    </style>
    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="#" class="link">{{ __('homepage.home') }}</a></li>
                <li class="item-link"><span>{{ __('homepage.nav-bar.all-categories') }}</span></li>
            </ul>
        </div>
        <div class="row">

            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 main-content-area">

{{--                <div class="banner-shop">--}}
{{--                    <a href="#" class="banner-link">--}}
{{--                        <figure><img src="{{ asset('assets/images/shop-banner.jpg') }}" alt=""></figure>--}}
{{--                    </a>--}}
{{--                </div>--}}

                <div class="wrap-shop-control">

                    <h1 class="shop-title">{{ __('homepage.nav-bar.all-categories') }}</h1>

                    <div class="wrap-right">

                        <div class="sort-item orderby ">
                            <select name="orderby" class="use-chosen" wire:model="sorting">
                                <option value="default" selected="selected">{{ __('shop.sorting.default') }}</option>
                                <option value="date">{{ __('shop.sorting.newness') }}</option>
                                <option value="price">{{ __('shop.sorting.low-to-high') }}</option>
                                <option value="price-desc">{{ __('shop.sorting.high-to-low') }}</option>
                            </select>
                        </div>

                        <div class="sort-item product-per-page">
                            <select name="post-per-page" class="use-chosen" wire:model="pagesize">
                                <option value="12" selected="selected">{{ __('shop.page-size.12-per-page') }}</option>
                                <option value="16">{{ __('shop.page-size.16-per-page') }}</option>
                                <option value="18">{{ __('shop.page-size.18-per-page') }}</option>
                                <option value="21">{{ __('shop.page-size.21-per-page') }}</option>
                                <option value="24">{{ __('shop.page-size.24-per-page') }}</option>
                                <option value="30">{{ __('shop.page-size.30-per-page') }}</option>
                                <option value="32">{{ __('shop.page-size.32-per-page') }}</option>
                            </select>
                        </div>

                        <div class="change-display-mode">
                            <a href="#" class="grid-mode display-mode active"><i class="fa fa-th"></i>{{ __('shop.grid') }}</a>
                            <a href="list.html" class="list-mode display-mode"><i class="fa fa-th-list"></i>{{ __('shop.list') }}</a>
                        </div>

                    </div>

                </div><!--end wrap shop control-->

                <div class="row">

                    <ul class="product-list grid-products equal-container">
                        @php
                            $witems = Cart::instance('wishlist')->content()->pluck('id');
                        @endphp
                        @foreach($products as $product)
                        <li class="col-lg-4 col-md-6 col-sm-6 col-xs-6 ">
                            <div class="product product-style-3 equal-elem ">
                                <div class="product-thumnail">
                                    <a href="{{ route('product-details', $product->slug) }}" title="{{ $product->name }}">
                                        <figure><img src="{{ asset('assets/images/products') }}/{{ $product->image }}" alt="{{ $product->name }}" style="max-height: 300px"></figure>
                                    </a>
                                </div>
                                <div class="product-info">
                                    <a href="{{ route('product-details', $product->slug) }}" class="product-name"><span>{{ $product->name }}</span></a>
                                    <div class="wrap-price"><span class="product-price">${{ $product->regular_price }}</span></div>
                                    <a href="#" class="btn add-to-cart" wire:click.prevent="store({{ $product->id }}, '{{ $product->name }}', {{ $product->regular_price }})">{{ __('shop.add-to-cart') }}</a>
                                    <div class="product-wish">
                                        @if($witems->contains($product->id))
                                            <a href="#" wire:click.prevent="removeFromWishlist({{ $product->id }})"><i class="fa fa-heart fill-heart"></i></a>
                                        @else
                                            <a href="#" wire:click.prevent="addToWishlist({{ $product->id }}, '{{ $product->name }}', {{ $product->regular_price }})"><i class="fa fa-heart"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>

                </div>

                <div class="wrap-pagination-info">
                    {{ $products->links() }}
{{--                    <ul class="page-numbers">--}}
{{--                        <li><span class="page-number-item current" >1</span></li>--}}
{{--                        <li><a class="page-number-item" href="#" >2</a></li>--}}
{{--                        <li><a class="page-number-item" href="#" >3</a></li>--}}
{{--                        <li><a class="page-number-item next-link" href="#" >Next</a></li>--}}
{{--                    </ul>--}}
{{--                    <p class="result-count">Showing 1-8 of 12 result</p>--}}
                </div>
            </div><!--end main products area-->

            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 sitebar">
                <div class="widget mercado-widget categories-widget">
                    <h2 class="widget-title">{{ __('homepage.nav-bar.all-categories') }}</h2>
                    <div class="widget-content">
                        <ul class="list-category">
                            @foreach($categories as $category)
                                <li class="category-item {{ count($category->subcategories) > 0 ? 'has-child-cate' : '' }}">
                                    <a href="{{ route('product-category', $category->slug) }}" class="cate-link">{{ $category->name }}</a>
                                    @if(count($category->subcategories) > 0)
                                        <span class="toggle-control">+</span>
                                        <ul class="sub-cate">
                                            @foreach($category->subcategories as $sub_category)
                                                <li class="category-item">
                                                    <a href="{{ route('product-category', ['category_slug' => $category->slug, 'sub_category_slug' => $sub_category->slug]) }}" class="cat-link"><i class="fa fa-caret-right"></i>{{ $sub_category->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div><!-- Categories widget-->

                <div class="widget mercado-widget filter-widget brand-widget">
                    <h2 class="widget-title">{{ __('shop.brand') }}</h2>
                    <div class="widget-content">
                        <ul class="list-style vertical-list list-limited" data-show="6">
                            <li class="list-item"><a class="filter-link active" href="#">Зедазені</a></li>
                            <li class="list-item"><a class="filter-link " href="#">Натахтарі</a></li>
                            <li class="list-item"><a class="filter-link " href="#">Саоджахо</a></li>
                            <li class="list-item"><a class="filter-link " href="#">Сакартвело</a></li>
                            <li class="list-item"><a class="filter-link " href="#">Ликані</a></li>
                            <li class="list-item"><a class="filter-link " href="#">Боржомі</a></li>
{{--                            <li class="list-item default-hiden"><a class="filter-link " href="#">Printer & Ink</a></li>--}}
{{--                            <li class="list-item default-hiden"><a class="filter-link " href="#">CPUs & Prosecsors</a></li>--}}
{{--                            <li class="list-item default-hiden"><a class="filter-link " href="#">Sound & Speaker</a></li>--}}
{{--                            <li class="list-item default-hiden"><a class="filter-link " href="#">Shop Smartphone & Tablets</a></li>--}}
{{--                            <li class="list-item"><a data-label='Show less<i class="fa fa-angle-up" aria-hidden="true"></i>' class="btn-control control-show-more" href="#">Show more<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>--}}
                        </ul>
                    </div>
                </div><!-- brand widget-->

                <div class="widget mercado-widget filter-widget price-filter">
                    <h2 class="widget-title">{{ __('shop.price') }} <span class="text-danger">${{ $min_price }} - ${{ $max_price }}</span></h2>
                    <div class="widget-content">
                        <div id="slider" wire:ignore></div>
                    </div>
                </div><!-- Price-->

                <div class="widget mercado-widget filter-widget" style="margin-top: 50px">
                    <h2 class="widget-title">{{ __('shop.weight') }}</h2>
                    <div class="widget-content">
                        <ul class="list-style vertical-list has-count-index">
                            <li class="list-item"><a class="filter-link " href="#">0.025 <span>(217)</span></a></li>
                            <li class="list-item"><a class="filter-link " href="#">0.050 <span>(179)</span></a></li>
                            <li class="list-item"><a class="filter-link " href="#">0.100 <span>(79)</span></a></li>
                            <li class="list-item"><a class="filter-link " href="#">0.250 <span>(283)</span></a></li>
                            <li class="list-item"><a class="filter-link " href="#">0.500 <span>(116)</span></a></li>
                            <li class="list-item"><a class="filter-link " href="#">1 кг <span>(29)</span></a></li>
                        </ul>
                    </div>
                </div><!-- Color -->

                <div class="widget mercado-widget filter-widget">
                    <h2 class="widget-title mt-5">{{ __('shop.volume') }}</h2>
                    <div class="widget-content">
                        <ul class="list-style inline-round ">
                            <li class="list-item"><a class="filter-link active" href="#">0.33</a></li>
                            <li class="list-item"><a class="filter-link " href="#">0.45</a></li>
                            <li class="list-item"><a class="filter-link " href="#">0.5</a></li>
                            <li class="list-item"><a class="filter-link " href="#">1</a></li>
                        </ul>
{{--                        <div class="widget-banner">--}}
{{--                            <figure><img src="{{ asset('assets/images/size-banner-widget.jpg') }}" width="270" height="331" alt=""></figure>--}}
{{--                        </div>--}}
                    </div>
                </div><!-- Size -->

                <div class="widget mercado-widget widget-product">
                    <h2 class="widget-title">{{ __('shop.popular.popular') }}</h2>
                    <div class="widget-content">
{{--                        <ul class="products">--}}
{{--                            <li class="product-item">--}}
{{--                                <div class="product product-widget-style">--}}
{{--                                    <div class="thumbnnail">--}}
{{--                                        <a href="detail.html" title="Radiant-360 R6 Wireless Omnidirectional Speaker [White]">--}}
{{--                                            <figure><img src="{{ asset('assets/images/products/digital_01.jpg') }}" alt=""></figure>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                    <div class="product-info">--}}
{{--                                        <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker...</span></a>--}}
{{--                                        <div class="wrap-price"><span class="product-price">$168.00</span></div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}

{{--                            <li class="product-item">--}}
{{--                                <div class="product product-widget-style">--}}
{{--                                    <div class="thumbnnail">--}}
{{--                                        <a href="detail.html" title="Radiant-360 R6 Wireless Omnidirectional Speaker [White]">--}}
{{--                                            <figure><img src="{{ asset('assets/images/products/digital_17.jpg') }}" alt=""></figure>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                    <div class="product-info">--}}
{{--                                        <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker...</span></a>--}}
{{--                                        <div class="wrap-price"><span class="product-price">$168.00</span></div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}

{{--                            <li class="product-item">--}}
{{--                                <div class="product product-widget-style">--}}
{{--                                    <div class="thumbnnail">--}}
{{--                                        <a href="detail.html" title="Radiant-360 R6 Wireless Omnidirectional Speaker [White]">--}}
{{--                                            <figure><img src="{{ asset('assets/images/products/digital_18.jpg') }}" alt=""></figure>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                    <div class="product-info">--}}
{{--                                        <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker...</span></a>--}}
{{--                                        <div class="wrap-price"><span class="product-price">$168.00</span></div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}

{{--                            <li class="product-item">--}}
{{--                                <div class="product product-widget-style">--}}
{{--                                    <div class="thumbnnail">--}}
{{--                                        <a href="detail.html" title="Radiant-360 R6 Wireless Omnidirectional Speaker [White]">--}}
{{--                                            <figure><img src="{{ asset('assets/images/products/digital_20.jpg') }}" alt=""></figure>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                    <div class="product-info">--}}
{{--                                        <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker...</span></a>--}}
{{--                                        <div class="wrap-price"><span class="product-price">$168.00</span></div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}

{{--                        </ul>--}}
                    </div>
                </div><!-- brand widget-->

            </div><!--end sitebar-->

        </div><!--end row-->

    </div><!--end container-->

</main>

@push('scripts')
    <script>
        var slider = document.getElementById('slider');
        noUiSlider.create(slider, {
            start:[1,1000],
            connect:true,
            range:{
                'min' : 1,
                'max' : 1000
            },
            pips:{
                mode:'steps',
                stepped:true,
                density:4
            }
        });

        slider.noUiSlider.on('update', function (values) {
            @this.set('min_price', values[0]);
            @this.set('max_price', values[1]);
        });
    </script>
@endpush
