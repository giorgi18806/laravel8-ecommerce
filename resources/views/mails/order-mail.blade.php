<!doctype html>
<html lang="uk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ __('mail-order.order-confirmation') }}</title>
</head>
<body>
    <p>{{ __('mail-order.hi') }} {{ $order->firstname }} {{ $order->lastname }}</p>
    <p>{{ __('mail-order.order-placed') }}</p>
    <br>

    <table style="width: 600px; text-align: right">
        <thead>
            <tr>
                <th>{{ __('mail-order.image') }}</th>
                <th>{{ __('mail-order.name') }}</th>
                <th>{{ __('mail-order.quantity') }}</th>
                <th>{{ __('mail-order.price') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($order->orderItems as $item)
                <tr>
                    <td><img src="{{ asset('assets/images/products') }}/{{ $item->product->image }}"
                             alt="{{ $item->product->name }}" width="100"></td>
                    <td>{{ $item->product->name }}</td>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ $item->price * $item->quantity }} ₴</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="3" style="border-top: 1px solid #ccc;"></td>
                <td style="font-size: 15px; font-weight: bold; border-top: 1px solid #ccc;">{{ __('mail-order.subtotal') }} : {{ $order->subtotal }} ₴</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td style="font-size: 15px; font-weight: bold;">{{ __('mail-order.tax') }} : {{ $order->tax }} ₴</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td style="font-size: 15px; font-weight: bold;">{{ __('mail-order.shipping') }} : {{ __('mail-order.free-shipping') }}</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td style="font-size: 22px; font-weight: bold;">{{ __('mail-order.total') }} : {{ $order->total }} ₴</td>
            </tr>
        </tbody>
    </table>
</body>
</html>
