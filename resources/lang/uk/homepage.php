<?php

return [
    'home' => 'Головна',
    'nav-bar' => [
        'hotline' => 'Гаряча лінія',
        'search-here' => 'Шукати тут',
        'all-categories' => 'Усі категорії',
        'account' => [
            'my-account' => 'Мій обліковий запис',
            'dashboard' => 'Панель керування',
            'logout' => 'Вийти',
        ],
        'auth' => [
            'login' => 'Увійти',
            'register' => 'Реєстрація',
        ],
        'actions' => [
            'weekly-featured' => 'Товар тижня',
            'hot-sale-items' => 'Гарячі товари',
            'top-new-items' => 'Топ новинок',
            'top-selling' => 'Топ продажів',
            'top-rated-items' => 'Топ товарів',
        ],
        'menu' => [
            'about-us' => 'Про компанію',
            'shop' => 'Магазин',
            'cart' => 'Кошик',
            'checkout' => 'Оформлення замовлення',
            'contact-us' => 'Зв\'яжіться з нами',
            ],
    ],

    'footer' => [
        'subscribe-1' => 'Підпишіться на нашу розсилку і',
        'subscribe-2' => 'швидше дізнайтеся про акції',
        'subscribe-placeholder' => 'Введіть свою електронну адресу',
        'subscribe-send' => 'Надіслати',
        'information' => 'Информація',
        'about' => 'Про компанію',
        'contacts' => 'Контакти',
        'sale' => 'Акції',
        'help' => 'Допомога',
        'delivery' => 'Доставка і Самовивезення',
        'payment' => 'Оплата',
        'return' => 'Повернення і обмін',
        'blog' => 'Блог',
        'privacy' => 'Політика обробки персональних даних',
        'terms-use' => 'Умови використання'
    ],
];
