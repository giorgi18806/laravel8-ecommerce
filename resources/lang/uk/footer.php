<?php

return [

    'free-shipping' => 'Безкоштовна доставка',
    'free-over' => 'Безкоштовно при замовленні від 1000 грн',
    'guarantee' => 'Гарантія',
    'money-back' => '3 дні повернення грошей',
    'safe-payment' => 'Безпечна оплата',
    'safe-online' => 'Ваш онлайн-платеж безпечний',
    'support' => 'Онлайн-підтримка',
    'support-time' => 'Ми маємо підтримку 24/7',
    'offer' => 'Спеціальна пропозиція',
    'gift' => 'Отримайте подарунок',
    'return-order' => 'Повернення замовлення',
    'return-days' => 'Повернення протягом 7 днів',
    'payments' => 'Ми використовуємо безпечні платежі',
    'social' => 'Соціальні мережі',

    'contact-details' => [
        'details' => 'Контактні Дані',
        'address' => 'Україна 33013, Рівенська обл., м. Рівно, вул. Степова 46/2',
        'hotline' => 'Гаряча лінія',
        'call-us' => 'Телефонуйте нам на гарячу лінію',
    ],

    'subscribe' => [
        'sign-up' => 'Підписатися на розсилку',
        'enter-email' => 'Введіть електрону адресу',
        'subscribe' => 'Підписатися',
    ],

    'copyright' => [
        'copyright' => 'Copyright © 2022 Сакартвело. Всі права захищені',
        'about-us' => 'Про компанію',
        'privacy' => 'Політика конфіденційності',
        'terms' => 'Правила та умови',
        'return' => 'Політика повернення',
    ],
];
