<?php

return [
    'description' => [
        'description' => 'опис',

    ],
    'additional' => [
        'additional' => 'Додаткова інформація',
        'weight' => 'Вага',
        'volume' => 'Об\'єм',

    ],
    'reviews' => [
        'reviews' => 'відгуки',
        'review-for' => 'Додати відгук для',
        'rated' => 'оцінено',
        'out-of' => 'з',
        'not-published' => 'Ваша електронна адреса не буде опублікована.',
        'required' => 'Обов’язкові поля позначені',
        'rating' => 'Ваш рейтинг',
        'name' => 'Ім\'я',
        'email' => 'Електронна пошта',
        'your-review' => 'Ваш відгук',
    ],
    'detail' => 'Подробиці',
    'review' => 'відгук',
    'add-to-cart' => 'Додати в кошик',
    'availability' => 'Наявність',
    'add-compare' => 'Додати Порівняти',
    'add-wishlist' => 'Додати до Списку Бажань',
    'quantity' => 'Кількість',
    'related' => 'Супутні товари',
    'submit' => 'Відправити',
];
