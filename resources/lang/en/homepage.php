<?php

return [
    'home' => 'home',
    'nav-bar' => [
        'hotline' => 'Hotline',
        'search-here' => 'Search here',
        'all-categories' => 'All Categories',
        'account' => [
            'my-account' => 'My Account',
            'dashboard' => 'Dashboard',
            'logout' => 'Logout',
        ],
        'auth' => [
            'login' => 'Login',
            'register' => 'Register',
        ],
        'actions' => [
            'weekly-featured' => 'Weekly Featured',
            'hot-sale-items' => 'Hot Sale items',
            'top-new-items' => 'Top new items',
            'top-selling' => 'Top Selling',
            'top-rated-items' => 'Top rated items',
        ],
        'menu' => [
            'about-us' => 'About Us',
            'shop' => 'Shop',
            'cart' => 'Cart',
            'checkout' => 'Checkout',
            'contact-us' => 'Contact Us',
            ],
    ],
    'parallax' => [
        'slogan' => 'Знижки на всі кекси 70%',
        'more' => 'ДІЗНАТИСЯ БІЛЬШЕ'
    ],
    'footer' => [
        'subscribe-1' => 'Підпишіться на нашу розсилку і',
        'subscribe-2' => 'швидше дізнайтеся про акції',
        'subscribe-placeholder' => 'Введіть свою електронну адресу',
        'subscribe-send' => 'Надіслати',
        'information' => 'Информація',
        'about' => 'Про компанію',
        'contacts' => 'Контакти',
        'sale' => 'Акції',
        'help' => 'Допомога',
        'delivery' => 'Доставка і Самовивезення',
        'payment' => 'Оплата',
        'return' => 'Повернення і обмін',
        'blog' => 'Блог',
        'privacy' => 'Політика обробки персональних даних',
        'terms-use' => 'Умови використання'
    ],
];
