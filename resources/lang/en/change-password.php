<?php

return [

    'change-password' => 'Change Password',
    'current-password' => 'Current Password',
    'new-password' => 'New Password',
    'confirm-password' => 'Confirm Password',
    'submit' => 'Submit',
];
