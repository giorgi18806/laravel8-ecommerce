<?php

return [
    'description' => [
        'description' => 'description',
        'newness' => 'Sort by newness',
        'low-to-high' => 'Sort by price: low to high',
        'high-to-low' => 'Sort by price: high to low',
    ],
    'additional' => [
        'additional' => 'Additional Information',
        'weight' => 'Weight',
        'volume' => 'Volume',

    ],
    'reviews' => [
        'reviews' => 'reviews',
        'review-for' => 'Add review for',
        'rated' => 'Rated',
        'out-of' => 'out of',
        'not-published' => 'Your email address will not be published.',
        'required' => 'Required fields are marked',
        'rating' => 'Your rating',
        'name' => 'Name',
        'email' => 'Email',
        'your-review' => 'Your review',
    ],
    'detail' => 'Detail',
    'review' => 'review',
    'add-to-cart' => 'Add to Cart',
    'availability' => 'Availability',
    'add-compare' => 'Add Compare',
    'add-wishlist' => 'Add Wishlist',
    'quantity' => 'Quantity',
    'related' => 'Related Products',
    'submit' => 'Submit',

];
