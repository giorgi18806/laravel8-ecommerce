<?php

return [

    'categories' => 'Categories',
    'attributes' => 'Attributes',
    'products' => 'Products',
    'manage-home-slider' => 'Manage Home Slider',
    'manage-home-categories' => 'Manage Home Categories',
    'sale-setting' => 'Sale Setting',
    'all-coupon' => 'All Coupon',
    'all-orders' => 'All Orders',
    'contact-messages' => 'Contact Messages',
    'settings' => 'Settings',
];
