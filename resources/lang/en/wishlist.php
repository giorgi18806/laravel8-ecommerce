<?php

return [

    'home' => 'home',
    'wishlist' => 'Wishlist',
    'no-items' => 'No items in wishlist',
    'move-to-cart' => 'Move To Cart',
    'items' => 'items',
];
