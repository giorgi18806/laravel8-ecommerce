<?php

return [

    'add-to-cart' => 'Item successfully added to cart',
    'cart-removed' => 'Item has been successfully removed',
    'cart-moved' => 'Item has been moved to cart',
    'cart-saved' => 'Item has been saved for later',
    'cart-remove-saved' => 'Item has been removed from save for later',
    'coupon-invalid' => 'Coupon code is invalid',
];
