<?php

return [
    'admin' => [
        'total-revenue' => 'Total Revenue',
        'updated-now' => 'Updated Now',
        'total-sales' => 'Total Sales',
        'today-revenue' => 'Today Revenue',
        'today-sales' => 'Today Sales',
        'latest-order' => 'Latest Order',
        'order_Id' => 'OrderId',
        'subtotal' => 'Subtotal',
        'contact-details' => 'Contact Details',
        'discount' => 'Discount',
        'tax' => 'Tax',
        'total' => 'Total',
        'first-name' => 'First Name',
        'last-name' => 'Last Name',
        'mobile' => 'Mobile',
        'email' => 'Email',
        'zipcode' => 'Zipcode',
        'status' => 'Status',
        'order-date' => 'Order Date',
        'action' => 'Action',
        'details' => 'Details',
    ],
    'user' => [
        'total-cost' => 'Total Cost',
        'total-purchase' => 'Total Purchase',
        'total-delivered' => 'Total Delivered',
        'total-canceled' => 'Total Canceled',
    ],
];
