<?php

return [

    'name' => 'Attribute Name',
    'id' => 'Id',
    'created_at' => 'Created At',
    'none' => 'None',
    'action' => 'Action',
    'add-new' => 'Add New',
    'add-new-attribute' => 'Add New Attribute',
    'all-attributes' => 'All Attributes',
    'edit-attribute' => 'Edit Attribute',
    'delete-attribute' => 'Are you sure, You want to delete this attribute?',
    'submit' => 'Submit',
    'update' => 'Update',
];
