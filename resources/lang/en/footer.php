<?php

return [

    'free-shipping' => 'Free Shipping',
    'free-over' => 'Free On Order Over UAH1000',
    'guarantee' => 'Guarantee',
    'money-back' => '3 Days Money Back',
    'safe-payment' => 'Safe Payment',
    'safe-online' => 'Safe your online payment',
    'support' => 'Online Support',
    'support-time' => 'We Have Support 24/7',
    'offer' => 'Special Offer',
    'gift' => 'Get a gift',
    'return-order' => 'Order Return',
    'return-days' => 'Return within 7 days',
    'payments' => 'We Using Safe Payments',
    'social' => 'Social network',

    'contact-details' => [
        'details' => 'Contact Details',
        'address' => 'Україна 33013, Рівенська обл., м. Рівно, вул. Степова 46/2',
        'hotline' => 'Hotline',
        'call-us' => 'Call us on the hotline',
    ],

    'subscribe' => [
        'sign-up' => 'Subscribe to the newsletter',
        'enter-email' => 'Enter your email address',
        'subscribe' => 'Subscribe',
    ],

    'copyright' => [
        'copyright' => 'Copyright © 2022 Sakartvelo. All rights reserved',
        'about-us' => 'About Us',
        'privacy' => 'Privacy Policy',
        'terms' => 'Terms & Conditions',
        'return' => 'Return Policy',
    ],
];
