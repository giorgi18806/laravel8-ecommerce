<?php

return [
    'profile' => 'Profile',
    'update-profile' => 'Update Profile',
    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone Number',
    'city' => 'City',
    'province' => 'Province',
    'country' => 'Country',
    'zipcode' => 'Zip Code',
    'address' => 'Address',
    'update' => 'Update',
];
