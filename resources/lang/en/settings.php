<?php

return [

    'settings' => 'Settings',
    'email' => 'Email',
    'phone' => 'Phone',
    'phone2' => 'Phone2',
    'address' => 'Address',
    'map' => 'Map',
    'twitter' => 'Twitter',
    'facebook' => 'Facebook',
    'pinterest' => 'Pinterest',
    'instagram' => 'Instagram',
    'youtube' => 'Youtube',
    'submit' => 'Save',
];
