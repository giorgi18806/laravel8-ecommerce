<?php

return [
    'order-confirmation' => 'Order Confirmation',
    'hi' => 'Hi',
    'image' => 'Image',
    'name' => 'Name',
    'quantity' => 'Quantity',
    'price' => 'Price',
    'subtotal' => 'Subtotal',
    'total' => 'Total',
    'tax' => 'Tax',
    'shipping' => 'Shipping',
    'free-shipping' => 'Free Shipping',
    'comment' => 'Comment',
    'submit' => 'Submit',
    'contact-messages' => 'Contact Messages',
    'contact-details' => 'Contact Details',
    'address' => 'Address',
    'address1' => 'Україна 33013, Рівенська обл., м. Рівно, вул. Степова 46/2',
];
