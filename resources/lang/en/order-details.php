<?php

return [

    'ordered_items' => [
        'ordered-items' => 'Ordered Items',
        'order-details' => 'Order Details',
        'order-id' => 'Order Id',
        'order-date' => 'Order Date',
        'order-status' => 'Order Status',
        'delivered-date' => 'Delivery Date',
        'canceled-date' => 'Cancellation Date',
        'cancel-order' => 'Cancel Order',
        'all-orders' => 'All Orders',
        'my-orders' => 'My Orders',
        'order-summary' => 'Order Summary',
        'order-subtotal' => 'Subtotal',
        'order-tax' => 'Tax',
        'order-shipping' => 'Shipping',
        'order-total' => 'Total',
    ],
    'billing_details' => [
        'billing-details' => 'Billing Details',
        'first-name' => 'First Name',
        'last-name' => 'Last Name',
        'phone' => 'Phone',
        'email' => 'Email',
        'line1' => 'Line1',
        'line2' => 'Line2',
        'city' => 'City',
        'province' => 'Province',
        'country' => 'Country',
        'zipcode' => 'Zipcode',
    ],
    'shipping_details' => [
        'shipping-details' => 'Shipping Details'
    ],
    'transaction' => [
        'transaction' => 'Transaction',
        'transaction-mode' => 'Transaction Mode',
        'transaction-status' => 'Status',
        'transaction-date' => 'Transaction Date',
    ],

];
