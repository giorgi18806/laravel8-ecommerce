<?php

return [

    'name' => 'Category Name',
    'slug' => 'Slug',
    'category-slug' => 'Category Slug',
    'parent-category' => 'Parent Category',
    'id' => 'Id',
    'none' => 'None',
    'sub_category' => 'Sub Category',
    'action' => 'Action',
    'add-new' => 'Add New',
    'add-new-category' => 'Add New Category',
    'all-categories' => 'All Categories',
    'edit-category' => 'Edit Category',
    'delete-category' => 'Are you sure, You want to delete this category?',
    'delete-sub_category' => 'Are you sure, You want to delete this subcategory?',
    'submit' => 'Submit',
    'update' => 'Update',
];
