<?php

return [
    'home' => 'home',
    'login' => 'Login',
    'loginto' => 'Log in to your account',
    'email' => 'Email Address',
    'email-placeholder' => 'Type your email address',
    'password' => 'Password',
    'confirm-password' => 'Confirm Password',
    'remember' => 'Remember me',
    'forgotten' => 'Forgotten password?',
    'submit' => 'Submit',
    'forgot-password' => 'Forgot Password',
    'email-link' => 'Email Password Reset Link',
    'reset-password' => 'Reset Password',
    'register' => 'Register',
    'create-account' => 'Create an account',
    'personal-information' => 'Personal information',
    'name' => 'Name',
    'your-name' => 'Your Name',
    'login-info' => 'Login Information',
];
