<?php

return [

    'name' => 'Name',
    'email' => 'Email',
    'phone' => 'Phone Number',
    'comment' => 'Comment',
    'submit' => 'Submit',
    'contact-messages' => 'Contact Messages',
    'contact-details' => 'Contact Details',
    'address' => 'Address',
    'address1' => 'Україна 33013, Рівенська обл., м. Рівно, вул. Степова 46/2',
];
