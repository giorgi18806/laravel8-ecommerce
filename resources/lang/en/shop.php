<?php

return [
    'sorting' => [
        'default' => 'Default sorting',
        'newness' => 'Sort by newness',
        'low-to-high' => 'Sort by price: low to high',
        'high-to-low' => 'Sort by price: high to low',
    ],
    'page-size' => [
        '12-per-page' => '12 per page',
        '16-per-page' => '16 per page',
        '18-per-page' => '18 per page',
        '21-per-page' => '21 per page',
        '24-per-page' => '24 per page',
        '30-per-page' => '30 per page',
        '32-per-page' => '32 per page',
    ],
    'popular' => [
        'popular' => 'Popular Products',
        'newness' => 'Sort by newness',
        'low-to-high' => 'Sort by price: low to high',
        'high-to-low' => 'Sort by price: high to low',
    ],
    'grid' => 'Grid',
    'list' => 'List',
    'add-to-cart' => 'Add to Cart',
    'brand' => 'Brand',
    'price' => 'Price',
    'volume' => 'Volume',
    'weight' => 'Weight',
];
