<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeCategorySeeder extends Seeder
{
//    protected $model = User::class;
    private $faker;

    public function run()
    {
        $sel_categories = [1,2,3,8];
        $today = date("Y-m-d H:i:s");
        $data = [
            'sel_categories' => json_encode($sel_categories),
            'no_of_products' => 6 ,
            'created_at'     => $today,
            'updated_at'     => $today,
        ];
        DB::table('home_categories')->insert($data);
    }
}
