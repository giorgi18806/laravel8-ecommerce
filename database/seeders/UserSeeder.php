<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
//    protected $model = User::class;
    private $faker;

    public function run()
    {

        $today = date("Y-m-d H:i:s");
        $data = [
            'name'              => 'Admin',
            'email'             => 'admin@admin.com' ,
            'password'          => Hash::make('password'), // password
            'utype'             => 'ADM',
            'created_at'        => $today,
            'email_verified_at' => $today,
        ];
        DB::table('users')->insert($data);
    }
}
