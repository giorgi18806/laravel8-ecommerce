<?php

namespace App\Http\Livewire;

use App\Mail\OrderMail;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Shipping;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;
use Cart;

class CheckoutComponent extends Component
{
    public $ship_to_different;

    public $first_name;
    public $last_name;
    public $email;
    public $mobile;
    public $line1;
    public $line2;
    public $city;
    public $province;
    public $country;
    public $zip_code;

    public $shipping_first_name;
    public $shipping_last_name;
    public $shipping_email;
    public $shipping_mobile;
    public $shipping_line1;
    public $shipping_line2;
    public $shipping_city;
    public $shipping_province;
    public $shipping_country;
    public $shipping_zip_code;

    public $payment_mode;
    public $thank_you;

    public function updated($fields)
    {
        $this->validateOnly($fields, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'mobile' => 'required|numeric',
            'line1' => 'required',
            'line2' => 'nullable',
            'city' => 'required',
            'province' => 'required',
            'country' => 'required',
            'zip_code' => 'required',
            'payment_mode' => 'required',
        ]);

        if($this->ship_to_different) {
            $this->validateOnly($fields, [
                'shipping_first_name' => 'required',
                'shipping_last_name' => 'required',
                'shipping_email' => 'required|email',
                'shipping_mobile' => 'required|numeric',
                'shipping_line1' => 'required',
                'shipping_line2' => 'nullable',
                'shipping_city' => 'required',
                'shipping_province' => 'required',
                'shipping_country' => 'required',
                'shipping_zip_code' => 'required',
            ]);
        }
    }

    public function placeOrder()
    {
        $this->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'mobile' => 'required|numeric',
            'line1' => 'required',
            'line2' => 'nullable',
            'city' => 'required',
            'province' => 'required',
            'country' => 'required',
            'zip_code' => 'required',
            'payment_mode' => 'required',
        ]);

        $order = new Order();
        $order->user_id = Auth::user()->id;
        $order->subtotal = session()->get('checkout')['subtotal'];
        $order->discount = session()->get('checkout')['discount'];
        $order->tax = session()->get('checkout')['tax'];
        $order->total = session()->get('checkout')['total'];
        $order->first_name = $this->first_name;
        $order->last_name = $this->last_name;
        $order->email = $this->email;
        $order->mobile = $this->mobile;
        $order->line1 = $this->line1;
        $order->line2 = $this->line2;
        $order->city = $this->city;
        $order->province = $this->province;
        $order->country = $this->country;
        $order->zip_code = $this->zip_code;
        $order->status = 'ordered';
        $order->is_shipping_different = $this->ship_to_different ? 1 : 0;
        $order->save();

        foreach(Cart::instance('cart')->content() as $item) {
            $orderItem = new OrderItem();
            $orderItem->product_id = $item->id;
            $orderItem->order_id = $order->id;
            $orderItem->price = $item->price;
            $orderItem->quantity = $item->qty;
            if($item->options) {
                $orderItem->options = serialize($item->options);
            }
            $orderItem->save();
        }

        if($this->ship_to_different) {
            $this->validate([
                'shipping_first_name' => 'required',
                'shipping_last_name' => 'required',
                'shipping_email' => 'required|email',
                'shipping_mobile' => 'required|numeric',
                'shipping_line1' => 'required',
                'shipping_line2' => 'nullable',
                'shipping_city' => 'required',
                'shipping_province' => 'required',
                'shipping_country' => 'required',
                'shipping_zip_code' => 'required',
            ]);

            $shipping = new Shipping();
            $shipping->order_id = $order->id;
            $shipping->first_name = $this->shipping_first_name;
            $shipping->last_name = $this->shipping_last_name;
            $shipping->email = $this->shipping_email;
            $shipping->mobile = $this->shipping_mobile;
            $shipping->line1 = $this->shipping_line1;
            $shipping->line2 = $this->shipping_line2;
            $shipping->city = $this->shipping_city;
            $shipping->province = $this->shipping_province;
            $shipping->country = $this->shipping_country;
            $shipping->zip_code = $this->shipping_zip_code;
            $shipping->save();
        }

        if($this->payment_mode == 'cod') {
            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->order_id = $order->id;
            $transaction->mode = 'cod';
            $transaction->status = 'pending';
            $transaction->save();
        }

        $this->thank_you = 1;
        Cart::instance('cart')->destroy();
        session()->forget('checkout');
        $this->sendOrderConfirmationMail($order);
    }


    public function sendOrderConfirmationMail($order)
    {
        Mail::to($order->email)->send(new OrderMail($order));
    }

    public function verifyForCheckout()
    {
        if(!Auth::check()) {
            return redirect()->route('login');
        }elseif($this->thank_you) {
            return redirect()->route('thank-you');
        }elseif(!session()->get('checkout')) {
            return redirect()->route('product-cart');
        }
    }

    public function render()
    {
        $this->verifyForCheckout();
        return view('livewire.checkout-component')->layout('layouts.base');
    }
}
